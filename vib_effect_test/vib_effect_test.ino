#define GPIO_VIB1 33
#define GPIO_VIB2 13
#define GPIO_COLD 21
#define GPIO_HEAT 14
#define DELTA_T_CREEPER_FUSE (1200 / 32)
#define DELTA_T_CREEPER_

void setup() {
  // put your setup code here, to run once:
    // Disbale all ports initially
  pinMode(GPIO_VIB1, OUTPUT);
  pinMode(GPIO_VIB2, OUTPUT);
  pinMode(GPIO_COLD, OUTPUT);
  pinMode(GPIO_HEAT, OUTPUT);
  analogWrite(GPIO_VIB1, 0);
  analogWrite(GPIO_VIB2, 0);
  analogWrite(GPIO_COLD, 0);
  analogWrite(GPIO_HEAT, 0);

}

void creeperSwell(int time) {
  float delta_t = time / 32;

  // Because teh following loop is missing one iteration, add it here
  analogWrite(GPIO_VIB1, 0);
  delay(delta_t);

  // i has to start at 1 so the analogWrite value does not evalute to -1
  for(int i = 1; i < 32; i++) {
    analogWrite(GPIO_VIB1, (i * 8) -1);
    delay(delta_t);
  }
  analogWrite(GPIO_VIB1, 0);
}

void loop() {
  // put your main code here, to run repeatedly:
  // First hit
  analogWrite(GPIO_VIB1, 255);
  analogWrite(GPIO_VIB2, 255);
  delay(100);

  analogWrite(GPIO_VIB1, 0);
  analogWrite(GPIO_VIB2, 0);
  delay(100);

  // Second hit
  analogWrite(GPIO_VIB1, 255);
  analogWrite(GPIO_VIB2, 255);
  delay(200);

  analogWrite(GPIO_VIB1, 0);
  analogWrite(GPIO_VIB2, 0);
  delay(2000);
}
