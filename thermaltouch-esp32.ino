#include <WiFi.h>
#include <WebSocketsServer.h>

#define GPIO_VIB1 13
#define GPIO_VIB2 33
#define GPIO_COLD 21
#define GPIO_HEAT 14

#define DELTA_T_CREEPER_FUSE (1200 / 32)
#define DELTA_T_VIB 100
#define HEAT_DEST 120
#define HEAT_POWER_TIME 2000

int vib1 = 0;
int vib2 = 0;
bool heatPowerOn = false;

int creeperSwellLevel = 0;
bool creeperSwellUp = false;
long millisSinceLastSwellUpdate = 0;
long millisDamageBlock = 0;
long heatPowerStartTime = 0;

bool explosionTriggered = false;
int nauseaStage = 0;

const char *ssid = "thermaltouch";
const char *password = "thermaltouch";

WebSocketsServer webSocket = WebSocketsServer(80);

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  String msg = "";
  for (size_t i = 0; i < len; i++) {
    msg += (char)data[i];
  }

  if (msg == "vib") {
    Serial.println("Hit");
    // This if prevents a delayed hit damage vibration after explosion
    if (millis() - millisDamageBlock > 200) {
      vib1 = DELTA_T_VIB;
      analogWrite(GPIO_VIB1, 255);
    }

  } else if (msg == "creeper_swell") {
    Serial.println("Creeper swells");
    creeperSwellUp = true;

  } else if (msg == "creeper_stop_swell") {
    Serial.println("Creeper stops swelling");
    creeperSwellUp = false;

  } else if (msg == "explosion") {
    Serial.println("Explosion");
    explosionTriggered = true;

  } else if (msg == "nausea_start") {
    Serial.println("Nausea start");
    nauseaInit();

  } else if (msg == "nausea_stop") {
    Serial.println("Nausea stop");
    nauseaStop();

  } else if (msg == "lightning") {
    Serial.println("Lightning");
    lightning();

  } else if (msg == "cold_start") {
    Serial.println("cold start");
    analogWrite(GPIO_COLD, 255);

  } else if (msg == "cold_stop") {
    Serial.println("cold stop");
    analogWrite(GPIO_COLD, 0);

  } else if (msg == "heat_start") {
    Serial.println("heat_start");
    analogWrite(GPIO_HEAT, 255);
    heatPowerOn = true;
    heatPowerStartTime = millis();

  } else if (msg == "heat_stop") {
    Serial.println("heat_stop");
    analogWrite(GPIO_HEAT, 0);
    heatPowerOn = false;

  } else {
    Serial.println("Invalid command");
  }
}

// Triggers every time a connection, disconnection or message was received
void onWebSocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length) {
  switch (type) {
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Disconnected!\n", num);
      break;
    case WStype_CONNECTED:
      {
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connection from %s\n", num, ip.toString().c_str());
      }
      break;
    case WStype_TEXT:
      handleWebSocketMessage((void *)num, payload, length);
      break;
    default:
      break;
  }
}

void explosion() {
  clearAll();
  analogWrite(GPIO_VIB1, 255);
  analogWrite(GPIO_VIB2, 255);
  delay(100);
  for (int i = 255; i > 0; i--) {
    analogWrite(GPIO_VIB1, i);
    analogWrite(GPIO_VIB2, i);
    delay(4);
  }
  millisDamageBlock = millis();
}

void clearAll() {
  vib1 = vib2 = 0;
  nauseaStage = 0;
  creeperSwellLevel = 0;
  heatPowerOn = false;
  creeperSwellUp = false;
  explosionTriggered = false;
  // Close all Transitors
  analogWrite(GPIO_VIB1, 0);
  analogWrite(GPIO_VIB2, 0);
  analogWrite(GPIO_COLD, 0);
  analogWrite(GPIO_HEAT, 0);
}

void nauseaInit() {
  clearAll();
  for (int i = 0; i < 256; i++) {
    analogWrite(GPIO_VIB2, i);
    analogWrite(GPIO_COLD, i);
    delay(5);
  }
  nauseaStage = 1;
}

void nauseaStop() {
  for (int i = 255; i >= 0; i--) {
    analogWrite(GPIO_VIB1, i);
    analogWrite(GPIO_VIB2, i);
    analogWrite(GPIO_COLD, i);
    analogWrite(GPIO_HEAT, i);
    delay(4);
  }
  clearAll();
}

void nauseaIT() {
  // Only allows values between 1 and 255 (both inclusive)
  nauseaStage = ((nauseaStage + 1) % 255) + 1;
  analogWrite(GPIO_VIB1, nauseaStage);
  analogWrite(GPIO_VIB2, 255 - nauseaStage);
  analogWrite(GPIO_COLD, nauseaStage < 128 ? 255 : 0);
  analogWrite(GPIO_HEAT, nauseaStage > 128 ? 255 : 0);
  delay(8);
}

void lightning() {
  clearAll();

  // First hit
  analogWrite(GPIO_VIB1, 255);
  analogWrite(GPIO_VIB2, 255);
  delay(100);

  analogWrite(GPIO_VIB1, 0);
  analogWrite(GPIO_VIB2, 0);
  delay(100);

  // Second hit
  analogWrite(GPIO_VIB1, 255);
  analogWrite(GPIO_VIB2, 255);
  delay(200);

  analogWrite(GPIO_VIB1, 0);
  analogWrite(GPIO_VIB2, 0);

  millisDamageBlock = millis();
}

void setup() {
  Serial.begin(9600);

  // Disbale all ports initially
  pinMode(GPIO_VIB1, OUTPUT);
  pinMode(GPIO_VIB2, OUTPUT);
  pinMode(GPIO_COLD, OUTPUT);
  pinMode(GPIO_HEAT, OUTPUT);
  analogWrite(GPIO_VIB1, 0);
  analogWrite(GPIO_VIB2, 0);
  analogWrite(GPIO_COLD, 0);
  analogWrite(GPIO_HEAT, 0);

  // Connect WLAN
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  Serial.println("Connected to WiFi");
  Serial.println(WiFi.localIP());

  // Init Websocket
  webSocket.begin();
  webSocket.onEvent(onWebSocketEvent);
}

void loop() {
  // Call websocket handler
  webSocket.loop();

  if (heatPowerOn && (millis() - heatPowerStartTime) > HEAT_POWER_TIME) {
    // End heat overclock mode
    analogWrite(GPIO_HEAT, HEAT_DEST);
    heatPowerOn = false;
  }

  if (nauseaStage > 0) {
    nauseaIT();

    // If nausea is present do not render other effects
    return;
  }

  if (explosionTriggered)
    explosion();

  if (vib1 > 0)
    if (--vib1 == 0)
      analogWrite(GPIO_VIB1, 0);

  // Check creeper status
  if ((millis() - millisSinceLastSwellUpdate) > DELTA_T_CREEPER_FUSE) {
    if (creeperSwellUp) {
      if (creeperSwellLevel < 255) {
        creeperSwellLevel = min(creeperSwellLevel + 8, 255);
        analogWrite(GPIO_VIB2, creeperSwellLevel);
      }
    } else {
      if (creeperSwellLevel > 0) {
        creeperSwellLevel = max(creeperSwellLevel - 16, 0);
        analogWrite(GPIO_VIB2, creeperSwellLevel);
      }
    }
    millisSinceLastSwellUpdate = millis();
  }
}
